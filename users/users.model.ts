import * as mongoose from 'mongoose';
import { validateCPF } from '../common/validator';
import * as bcrypt from 'bcrypt';
import { environment } from '../common/environment';

export interface User extends mongoose.Document {
    name: string,
    email: string,
    password: string,
    gender: string,
    profiles: string[],
    matches(password: string): boolean,
    hasAny(...profiles: string[]): boolean
    //O spread é para não precisar passar um array no argumento
    // podemos passar as strings separadas por vírgula que 
    // o spread junta como um array de string
}

export interface UserModel extends mongoose.Model<User> {
    findByEmail(email: string, projection?: string): Promise<User>
}

// Schema informa ao mongoose quais são os 
// metadados do documento
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 80,
        minlength: 3
    },
    email: {
        type: String,
        unique: true,
        match: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/,
        required: true
    },
    password: {
        type: String,
        select: false, // não exibe o campo em uma select comum
        required: true
    },
    gender: {
        type: String,
        required:false,
        enum: ['Male','Female']
    },
    cpf: {
        type: String,
        required: false,
        validate: {
            validator: validateCPF,
            message: `{PATH}: Invalid CPF. ({VALUE})`
        }
    },
    profiles: {
        type: [String],
        required: false
      }
});

userSchema.statics.findByEmail = function(email: string, projection: string) {
    return this.findOne({ email }, projection)
}

userSchema.methods.matches = function( password: string): boolean {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.hasAny = function(...profiles: string[]) : boolean {
    console.log("teste->", profiles);
    return profiles.some(profile => this.profiles.indexOf(profile) !== -1)
};

const hashPassword = (obj, next) => {
    bcrypt.hash(obj.password, environment.security.saltRounds)
    .then( hash => {
        obj.password = hash;
        next();
    })
    .catch(next)
};

const saveMiddleware = function(next) {
    const user: User = this;
    if(!user.isModified('password')) {
        next();
    } else {
        hashPassword(user, next);
    }
};

const updateMiddleware = function(next) {
    // utilizado tanto para "findOneAndUpdate" quanto para "findByIdAndUpdate"
    if(!this.getUpdate().password) {
        next();
    } else {
        hashPassword(this.getUpdate(), next);
    }
}

userSchema.pre('save', saveMiddleware);

userSchema.pre('findOneAndUpdate', updateMiddleware);

userSchema.pre('update', updateMiddleware);


// Model é classe que faz a real 
// manipulação dos dados
export const User = mongoose.model<User, UserModel>('User', userSchema);
