"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleError = (req, res, err, done) => {
    err.toJSON = () => {
        return {
            message: err.message
        };
    };
    done();
};
