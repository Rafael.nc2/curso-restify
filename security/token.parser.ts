import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';

import { User } from '../users/users.model';
import { environment } from '../common/environment';

export const tokenParser: restify.RequestHandler = (req, res, next) => {
    const token = extractToken(req);
    
    if(token) {
        jwt.verify(token, environment.security.apiSecret, applyBearer(req, next) );
    } else {
        next();
    }
}

function extractToken(req: restify.Request) {
    // Bearer (portador do token - esquema de autenticação)
    // Authorization: Bearer TOKEN

    let token = undefined;
    const authorization = req.header('authorization');
    if(authorization) {
        const parts: string[] = authorization.split(' ');

        if(parts.length === 2 && parts[0] === 'Bearer') {
            token = parts[1];
        }
    }

    return token;
}

// applyBearer = função que retorna uma outra função sem retorno (void)
// com a assinatura de (error, decoded)
function applyBearer(req: restify.Request, next): (error, decoded) => void {
    return (error, decoded) => {
        if(decoded) {
            User.findByEmail(decoded.sub)
                .then(user => {
                    if(user) {
                        // associar  usuário ao request
                        // cast para any - mesmo não fazendo, em runtime funcionaria normalmente
                        // (<any>req).authenticated = user;
                        req.authenticated = user; 
                    }
                    next();
                })
                .catch(next)
        } else {
            next();
        }
    }
}